from django.urls import reverse
from django.contrib.sites.shortcuts import get_current_site
from django.conf import settings
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import force_bytes, smart_str, DjangoUnicodeDecodeError
from django.contrib.auth.tokens import PasswordResetTokenGenerator

from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import IsAuthenticated

from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from .models import User
from .utils import EmailHelper
from .serializers import *
from .tasks import send_email_task

import jwt
import jsonpickle


class RegisterView(generics.GenericAPIView):
    serializer_class = UserSerializer
    
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        user_data = serializer.data
        user = User.objects.get(email=user_data['email'])
        site = get_current_site(request).domain
        token = RefreshToken.for_user(user)
        view = reverse('email-verify')
        url = "http://" + site + view + "?token=" + str(token)
        body = "Hi " + user.name + "\n Please use the link below to verify your account " + url
        
        data = {
            'subject': "Verify your email",
            "body": body,
            "to": user.email
        }
        
        send_email_task.delay(data)
        
        return Response({"message": "We've sent you an email to verify your account"}, status=status.HTTP_201_CREATED)
    
    
class EmailVerificationView(generics.GenericAPIView):
    serializer_class = EmailVerificationSerializer
    
    test_param = openapi.Parameter('token', openapi.IN_QUERY, description="token", type=openapi.TYPE_STRING)
    
    @swagger_auto_schema(manual_parameters=[test_param])
    def get(self, request):
        token = request.GET.get('token')
        
        try:
            payload = jwt.decode(token, settings.SECRET_KEY, algorithms="HS256")
            user = User.objects.get(id=payload["user_id"])
            
            if not user.is_verified:
                user.is_verified = True
                user.save()
                
            return Response({"message": "Successfully activation"}, status=status.HTTP_200_OK)
            
        except jwt.exceptions.ExpiredSignatureError as e:
            return Response({"error": "Activation Expire"}, status=status.HTTP_400_BAD_REQUEST)
        except jwt.exceptions.DecodeError as e:
            return Response({'error': "Invalid token"}, status=status.HTTP_400_BAD_REQUEST)
        

class LoginView(generics.GenericAPIView):
    serializer_class = LoginSerializer
    
    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        return Response(serializer.data, status=status.HTTP_200_OK)
    
    
class ResetPasswordView(generics.GenericAPIView):
    serializer_class = ResetPasswordSerializer
    
    def post(self, request, *args, **kwargs):
        email = request.data.get('email')
        
        if User.objects.filter(email=email).exists():
            user = User.objects.get(email=email)
            import time
            start_time = time.time()
            uidb64 = urlsafe_base64_encode(force_bytes(jsonpickle.encode(user)))
            print("--- encode uidb64 %s seconds ---" % (time.time() - start_time))
            token = PasswordResetTokenGenerator().make_token(user)
            site = get_current_site(request).domain
            url = reverse("password-reset-confirm", kwargs={
                "uidb64": uidb64,
                "token": token
            })
            link = "http://" + site + url
            body = "Hi " + user.name + "\n Please use the link below to reset your password: " + link
            data = {
                "subject": "Reset your password",
                "body": body,
                "to": user.email
            }
            
            send_email_task.delay(data)
            
            return Response({"message": "We've sent you an email to reset your password"}, status=status.HTTP_200_OK)
        
        
class CheckPasswordResetTokenView(APIView):
    
    def post(self, request, uidb64, token):
        try:
            import time
            start_time = time.time()
            obj = smart_str(urlsafe_base64_decode(uidb64))
            print("--- decode uidb64 %s seconds ---" % (time.time() - start_time))
            user_obj = jsonpickle.decode(obj)
            user = User.objects.get(id=user_obj.id)
            
            if not PasswordResetTokenGenerator().check_token(user, token):
                return Response({"error": "Token invalid, please request a new one"}, status=status.HTTP_400_BAD_REQUEST)
            
            return Response({"success": True, "uidb64": uidb64, "token": token}, status=status.HTTP_200_OK)
            
        except Exception as e:
            return Response({"error": "Token invalid, please request a new one"}, status=status.HTTP_400_BAD_REQUEST)


class SetNewPasswordView(generics.GenericAPIView):
    serializer_class = SetNewPasswordSerializer
    
    def patch(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        
        return Response({"message": "Password reset successfully"}, status=status.HTTP_200_OK)
    
    
class LogoutView(generics.GenericAPIView):
    serializer_class = LogoutSerializer
    permission_classes = (IsAuthenticated,)
    
    def post(self, request):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        
        return Response(status=status.HTTP_204_NO_CONTENT)