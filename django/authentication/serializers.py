from rest_framework import serializers
from rest_framework.exceptions import AuthenticationFailed
from rest_framework_simplejwt.tokens import RefreshToken

from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.utils.encoding import smart_str, force_bytes, DjangoUnicodeDecodeError
from django.contrib import auth
from django.contrib.auth.tokens import PasswordResetTokenGenerator

from .models import User
from types import SimpleNamespace

import jsonpickle


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    
    class Meta:
        model = User
        fields = ("email", "name", "password")
        
    def validate(self, attrs):
        email = attrs.get('email', '')
        name = attrs.get('name', '')
        password = attrs.get('password', '')
        
        if not name.isalnum():
            raise AuthenticationFailed("Name should only contain alphanumeric characters")
        
        return attrs
    
    def create(self, validated_data):
        return User.objects.create_user(**validated_data)
    
    
class EmailVerificationSerializer(serializers.ModelSerializer):
    token = serializers.CharField()
    
    class Meta:
        model = User
        fields = ("token",)
        
        
class LoginSerializer(serializers.ModelSerializer):
    email = serializers.CharField()
    password = serializers.CharField(write_only=True)
    name = serializers.CharField(read_only=True)
    tokens = serializers.CharField(read_only=True)
    
    class Meta:
        model = User
        fields = ("email", "password", "name", "tokens")
        
    def validate(self, attrs):
        email = attrs.get('email', '')
        password = attrs.get('password', '')
        
        user = auth.authenticate(email=email,password=password)        
        
        if not user:
            raise AuthenticationFailed("Invalid credentials, try again")
        if not user.is_active:
            raise AuthenticationFailed("Account disabled, contact admin")
        if not user.is_verified:
            raise AuthenticationFailed("Account is not verified")
        
        return {
            "email": user.email,
            "name": user.name,
            "tokens": user.tokens()
        }
        
        
class ResetPasswordSerializer(serializers.Serializer):
    email = serializers.CharField()    
    
    class Meta:
        fields = ("email",)
        
        
class SetNewPasswordSerializer(serializers.Serializer):
    password = serializers.CharField(write_only=True)
    uidb64 = serializers.CharField(write_only=True)
    token = serializers.CharField(write_only=True)
    
    class Meta:
        fields = ("password", "token", "uidb64")
        
    def validate(self, attrs):
        try:
            password = attrs.get('password', '')
            uidb64 = attrs.get('uidb64', '')
            token = attrs.get('token', '')
            
            obj = smart_str(urlsafe_base64_decode(uidb64))
            user_obj = jsonpickle.decode(obj)
            user = User.objects.get(id=user_obj.id)
            
            if not PasswordResetTokenGenerator().check_token(user, token):
                raise AuthenticationFailed("Invalid token, please request a new one")
            
            user.set_password(password)
            user.save()
            return user
            
        except Exception as e:
            raise AuthenticationFailed("Invalid token, please request a new one")
        

class LogoutSerializer(serializers.Serializer):
    refresh = serializers.CharField()
    
    class Meta:
        fields = ("refresh",)
        
    def validate(self, attrs):
        self.token = attrs.get('refresh', '')
        
        return attrs
    
    def save(self, **kwargs):
        try:
            RefreshToken(self.token).blacklist()
        except Exception as e:
            raise e
        
        
class UserViewSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = ("id", "is_superuser", "email", "name", "is_active", "is_verified", "is_staff", "created_at", "updated_at", "groups")